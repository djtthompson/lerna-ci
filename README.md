# Basic CI Example Using Lerna

## Local Setup

```sh
npm install
npm test
npm run build
```

## CI

Using gitlab pipelines each package extends variables defined by the root `.gitlab-ci.yml`.
